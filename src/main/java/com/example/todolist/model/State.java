package com.example.todolist.model;

public enum State {
    DRAFT, IN_PROGRESS, DONE, CANCELED;
}
