package com.example.todolist.model;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(schema = "todo")
public class Todo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="title")
    private String title;

    @Column(name="description")
    private String description;

    @Column(name = "state")
    private State state;
}
