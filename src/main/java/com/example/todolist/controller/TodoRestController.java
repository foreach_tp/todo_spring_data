package com.example.todolist.controller;

import com.example.todolist.model.State;
import com.example.todolist.model.Todo;
import com.example.todolist.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class TodoRestController {

    @Autowired
    private TodoService todoService;

    @GetMapping
    public List<Todo> getAllTodo(){
        return todoService.getAllTodo();
    }

    @GetMapping("/sorted")
    public List<Todo> getAllTodoSorted(){
        return todoService.getAllTodoSorted();
    }

    @GetMapping("/{id}")
    public Todo getTodoById(@PathVariable long id){
        return todoService.getTodoById(id).get();
    }

    @PostMapping
    public Todo createTodo(@RequestBody Todo todo){
        return todoService.createTodo(todo);
    }

    @DeleteMapping("/{id}")
    public void deleteTodo(@PathVariable long id){
        todoService.delete(id);
    }

    @PutMapping()
    public Todo updateTodo(@RequestBody Todo todo){
        return todoService.createTodo(todo);
    }

    @GetMapping("/state/{state}")
    public List<Todo> getTodoByState(@PathVariable String state){
        return todoService.getTodoByState(state);
    }
}
