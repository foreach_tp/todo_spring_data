package com.example.todolist.service;

import com.example.todolist.model.State;
import com.example.todolist.model.Todo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface TodoService {

    List<Todo> getAllTodo();

    List<Todo> getAllTodoSorted();

    Optional<Todo> getTodoById(long id);

    Todo createTodo(Todo todo);

    void delete(long id);

    List<Todo> getTodoByState(String state);
}
