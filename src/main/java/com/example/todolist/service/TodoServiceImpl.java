package com.example.todolist.service;

import com.example.todolist.model.State;
import com.example.todolist.model.Todo;
import com.example.todolist.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TodoServiceImpl implements TodoService {

    @Autowired
    private TodoRepository todoRepository;
    @Override
    public List<Todo> getAllTodo() {
        return todoRepository.findAll();
    }

    @Override
    public List<Todo> getAllTodoSorted() {
        return todoRepository.getTodoSorted();
    }

    @Override
    public Optional<Todo> getTodoById(long id) {
        return todoRepository.findById(id);
    }

    @Override
    public Todo createTodo(Todo todo) {
        return todoRepository.save(todo);
    }

    public void delete(long id){
        todoRepository.deleteById(id);
    }

    @Override
    public List<Todo> getTodoByState(String state){
            return todoRepository.findByState(State.valueOf(state));
    }
}
